const alerta = document.querySelector("#alerta");
const amount = document.querySelector("#amount");
const myTextArea = document.querySelector("#myTextArea");


alerta.classList.add("hidden")
function getRandomNumber(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
  }
  
  function getRandomDate() {
    const year = getRandomNumber(2023, 2024); 
    const month = getRandomNumber(1, 13); 
    const day = getRandomNumber(1, 32);
    return `${day < 10 ? '0' : ''}${day}-${month < 10 ? '0' : ''}${month}-${year}`;
  }
  
  function getRandomTime() {
    const hours = getRandomNumber(0, 24); 
    const minutes = getRandomNumber(0, 60); 
    const seconds = getRandomNumber(0, 60); 
    return `${hours < 10 ? '0' : ''}${hours}:${minutes < 10 ? '0' : ''}${minutes}:${seconds < 10 ? '0' : ''}${seconds}`;
  }
  let link;
  let file;
  function createInteractionsArray(n) {
    const interactions = [];
    const stringInteractions = []
    for (let i = 0; i < n; i++) {
      const interaction = {
        typeArtifact: 'Standard',
        artifact: getRandomNumber(1, 7), 
        results: {
          correct: getRandomNumber(1, 7), 
          incorrect: getRandomNumber(1, 7), 
          forAnswer:getRandomNumber(1, 7), 
        },
        elementArtifact: {
          cleamPoints: [],
          cleamPar: [],
          inputsValues: {
            inpEngInt1: '',
          },
        },
        date: getRandomDate(),
        hour: getRandomTime(),
        seconds: getRandomNumber(1, 2001),
        chapter: getRandomNumber(1, 7), 
        page: getRandomNumber(1, 31), 
      };
      interactions.push(interaction);
      stringInteractions.push(JSON.stringify(interaction))
    }
   link = document.createElement("a");
   // link.textContent='aaaa'
if(stringInteractions.length>200){
  const file = new Blob([stringInteractions], { type: 'text/plain' });
  link.href = URL.createObjectURL(file);
  link.download = "sample.json";
  link.click();

}
else{
  return interactions;

}

  }
  let totalContent = ''
  function prettyPrint(interactionsArray) {
    const batchSize = 5; // Tamaño del lote para procesar
   // let myobj;
    let prettyResults = ''; // Cadena temporal para almacenar resultados

    function processBatch(startIndex) {
      URL.revokeObjectURL(link.href);

  
        for (let i = startIndex; i < Math.min(startIndex + batchSize, interactionsArray.length); i++) {
            prettyResults += JSON.stringify(interactionsArray[i], null, 4) + ',';
        }

        // Actualiza el área de texto después de procesar el lote
         totalContent+= prettyResults;

        if (startIndex + batchSize < interactionsArray.length) {
            // Procesa el siguiente lote
            requestAnimationFrame(() => processBatch(startIndex + batchSize));
        }
    }

    // Comienza el procesamiento
    processBatch(0);
    myTextArea.value = totalContent

}


 amount.addEventListener("input",(e)=>{
    alerta.classList.add("hidden")
    myTextArea.value ="";
   
    const interactionsArray = createInteractionsArray(document.querySelector("#amount").value);
    if (interactionsArray != undefined){
    prettyPrint(interactionsArray)

} 

})
let boton = document.querySelector("#copy").addEventListener("click",(e)=>{
    myTextArea.select()
    document.execCommand("copy");
    alerta.classList.remove("hidden")

})